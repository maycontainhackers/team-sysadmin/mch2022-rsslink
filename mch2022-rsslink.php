<?php

/*
 * Plugin Name: Overwrite the RSS blog URL
 * Description: MCH2021 Sysadmin custom plugin
  * Author: Xesxen
 * Version: 1.1.0
 * */

const MCH_BLOG_PREFIX = '/#/Blog/';
const MCH_BLOG_PREVIEW_PREFIX = MCH_BLOG_PREFIX . 'preview/';

function fix_blog_url( $url, $post, $leavename=false ) {
    if ( $post->post_type == 'post' ) {
        if ($post->post_status !== 'publish') {
            $url = get_option("siteurl") . MCH_BLOG_PREVIEW_PREFIX . $post->ID;
        } else {
            $url = get_option("siteurl") . MCH_BLOG_PREFIX . $post->post_name;
        }
    }
    return $url;
}

add_filter( 'post_link', 'fix_blog_url', 10, 3 );

function fix_preview_blog_url( $url, $post, $leavename=false ) {
    if ( $post->post_type == 'post' ) {
        $url = get_option("siteurl") . MCH_BLOG_PREVIEW_PREFIX . $post->ID;
    }
    return $url;
}

add_filter( 'preview_post_link', 'fix_preview_blog_url', 10, 3);

/** Needed as the GUID might not match the article's actual URL (see fix_blog_url())  */
function mch_regenerate_guid($id) {
    global $wpdb;

    $post = get_post($id);

    if (str_contains($post->guid, MCH_BLOG_PREFIX) && !str_contains($post->guid, MCH_BLOG_PREVIEW_PREFIX)) {
        return;
    }

    $wpdb->update( $wpdb->posts, array( 'guid' => get_permalink( $id ) ), array( 'ID' => $id ) );
}

// Note: Deprecated, but the replacement does not get called?
add_filter( 'private_to_published', 'mch_regenerate_guid', 10, 3);

function mch_override_author($name) {
    if (is_admin()) {
        return $name;
    }

    $post = get_post();
    $categories = array_column(
        wp_get_object_terms($post->ID, 'category'),
        'name'
    );
    $categories = array_values(array_filter(
        $categories,
        fn($category) => str_starts_with(strtolower($category), 'team:'),
    ));
    sort($categories);

    return implode(', ', $categories);
}

add_filter( 'the_author', 'mch_override_author', 10, 3);
